"use strict";
var middleware = require("harp");

module.exports = function(options, imports, register){
  var debug = imports.debug("harp");
  debug("start");

  var app = imports.express;

  var rootDir = options.rootDir || process.cwd();

  debug(".useStatic");
  if (options.publicDir) {
    app.useStatic(app.static(options.publicDir));
  }
  app.useStatic(app.static(rootDir));
  app.useStatic(middleware.mount(rootDir));


  debug("register nothing");
  register(null, {});

};
